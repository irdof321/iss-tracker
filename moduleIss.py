# import pygame
import pygame    #to install
import time
import numpy
import math
from enum import Enum

#mode select
DISPLAYSURF = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
class modeSelect(Enum) :
     RADIAN = 1
     DEGREE = 2

# initialize game engine
pygame.init()

#path to map image file
#map_image = "GMT.png"
map_image = "carte.jpg"

#set display size
window_width=600
window_height=400

#scale
scale = 2

#map image size
map_width = 1650*scale
map_length = 1070*scale

# Open a window and set its size
size = (window_width, window_height)
screen = pygame.display.set_mode(size)

# Set title to the window
display = pygame.display.set_caption("ISS pointer")

#load map image
background_image = pygame.image.load(map_image).convert()
#set size map
background_image = pygame.transform.scale(background_image,(map_width,map_length))

#load pointer image
pointer = pygame.image.load('pointeur.png')
ponteur_size = 50
#set size pointer
pointer = pygame.transform.scale(pointer,(ponteur_size,ponteur_size))


lat_max_up = 70/360*(2*numpy.pi)
lat_max_down = 70/360*(2*numpy.pi)

#this Mercator projection suppose thatv the Earth is a sphere
def MercatorProjection(lat,longi) :
	if lat > lat_max_up :
		lat = lat_max_up
	elif lat < - lat_max_down :
		lat = -lat_max_down
	#Mercator projection
	x = (map_width/(2*numpy.pi)*(longi+numpy.pi))
	y =  map_length/2 - 0.5* numpy.tan(lat)/numpy.tan(lat_max_up)*map_length
	print ('(x,y) == (',x,',',y,')| (lat,longi) == (',lat/(2*numpy.pi)*360,',',longi/(2*numpy.pi)*360,')----------')
	return [x,y]



#space between images to reduce
shift_map  = 10

#put lat and longi in degree or rad
# lat = latitude
# longi = longitude
# mode : put modeSelect.DEGREE or 2 if putting lat & longi in degree
def displayISS(lat,longi,mode = modeSelect.RADIAN) :
	if(mode == modeSelect.DEGREE) :
		lat = lat/360 * 2*numpy.pi
		longi = longi/360 * 2*numpy.pi
		print ('je suis ici : ',lat,'...',longi)

	[x_m,y_m] = MercatorProjection(lat,longi)

  	## Border prevent overflow of image
  	# when the size is too big the map stop moving and the pointer start moving in the window
	if y_m < window_height/2 :
		screen.blit(background_image, [-x_m+window_width/2 ,0])										# image map center
		screen.blit(background_image, [-x_m+window_width/2 - map_width + shift_map ,0])						# image map left
		screen.blit(background_image, [-x_m+window_width/2 + map_width - shift_map,0])						# image map right
		screen.blit(pointer,(window_width/2 - ponteur_size/2,y_m - ponteur_size/2))			        			#pointer

	elif y_m > map_length - window_height/2 :
		screen.blit(background_image, [-x_m+window_width/2 ,map_length - window_height])						# image map center
		screen.blit(background_image, [-x_m+window_width/2 - map_width + shift_map ,map_length - window_height])		# image map left
		screen.blit(background_image, [-x_m+window_width/2 + map_width - shift_map,map_length - window_height])		# image map right
		screen.blit(pointer,(window_width/2 - ponteur_size/2,window_height - (map_length - y_m) + ponteur_size/2))		#pointer

	else :
		screen.blit(background_image, [-x_m+window_width/2 ,-y_m+window_height/2])							# image map center
		screen.blit(background_image, [-x_m+window_width/2 - map_width + shift_map ,-y_m+window_height/2])			# image map left
		screen.blit(background_image, [-x_m+window_width/2 + map_width - shift_map,-y_m+window_height/2])			# image map right
		screen.blit(pointer,(window_width/2 - ponteur_size/2,window_height/2 - ponteur_size/2))			        	#pointer

	pygame.display.flip()


deg =  numpy.linspace(-70,70,20)
for i in  deg :
	displayISS(i,0,modeSelect.DEGREE)
	time.sleep(1)

#time.sleep(10)






